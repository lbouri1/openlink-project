from django.contrib import admin
from django.contrib.admin.options import get_content_type_for_model
from django.urls import reverse
from django.utils.html import format_html
from django.utils.translation import ugettext


from .models import Project
from .models import Folder
from .models import Experiment
from .models import Screen
from .models import Dataset
from .models import Documentation
from .models import Link
from .models import Download
from .models import Publication
from .models import Profile
from .models import Token
from .models import Tool
from .models import Image





class ViewOnSiteModelAdmin(admin.ModelAdmin):
    class Media:
        css = {
            'all': ('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',)
        }



admin.site.register(Project)
admin.site.register(Folder)
admin.site.register(Experiment)
admin.site.register(Screen)
admin.site.register(Dataset)
admin.site.register(Documentation)
admin.site.register(Link)
admin.site.register(Download)
admin.site.register(Publication)
admin.site.register(Profile)
admin.site.register(Token)
admin.site.register(Tool)
admin.site.register(Image)