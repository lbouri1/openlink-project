from django.apps import AppConfig


class OpenlinkAppConfig(AppConfig):
    name = 'openlink_app'
