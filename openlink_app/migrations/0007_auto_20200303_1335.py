# Generated by Django 3.0.3 on 2020-03-03 13:35

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('openlink_app', '0006_auto_20200303_1327'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='project',
            name='creator',
        ),
        migrations.AddField(
            model_name='project',
            name='author',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
