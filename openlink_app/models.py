from datetime import datetime
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver



class Publication(models.Model):
    doi = models.CharField(max_length=100)
    pmid = models.CharField(max_length=100, blank=True, null=True)
    pmcid = models.CharField(max_length=100, blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    version = models.CharField(max_length=100, blank=True, null=True)
    def __str__(self):
        return self.doi

class Download(models.Model):
    link = models.CharField(max_length=100)
    url = models.URLField(max_length=200, blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    comment = models.CharField(max_length=1000, blank=True, null=True)

    def __str__(self):
        return self.link

class Link(models.Model):
    url = models.URLField(max_length=200)
    type = models.CharField(max_length=100, blank=True, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    comment = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.url

class Documentation(models.Model):
    url = models.CharField(max_length=100, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)
    comment = models.CharField(max_length=100, blank=True, null=True)
    cmd = models.CharField(max_length=100, blank=True, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    version = models.CharField(max_length=100, blank=True, null=True)
    def __str__(self):
        return self.url


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    def __str__(self):
        return self.doi


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

class Tool(models.Model):
    tool = models.CharField(max_length=100, blank=True, null=True)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    def __str__(self):
        return self.tool

class Token(models.Model):
    token = models.CharField(max_length=100, blank=True, null=True)
    tool = models.ForeignKey(Tool, on_delete=models.CASCADE)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    def __str__(self):
        return self.token

class Image(models.Model):
    name = models.CharField(max_length=100, blank=True)
    url = models.URLField(max_length=200, blank=True, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    version = models.CharField(max_length=100, blank=True, null=True)
    def __str__(self):
        return self.name

class Dataset(models.Model):
    name = models.CharField(max_length=100, blank=True)
    datePublished = models.DateTimeField(default=datetime.now, blank=True, null=True)
    description = models.CharField(max_length=100, blank=True, null=True)
    keywords = models.CharField(max_length=100, blank=True, null=True)
    url = models.URLField(max_length=200, blank=True, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    image = models.ManyToManyField(Image, blank=True)
    version = models.CharField(max_length=100, blank=True, null=True)
    def __str__(self):
        return self.name


class Experiment(models.Model):
    name = models.CharField(max_length=100, blank=True)
    datePublished = models.DateTimeField(default=datetime.now, blank=True, null=True)
    description = models.CharField(max_length=100, blank=True, null=True)
    keywords = models.CharField(max_length=100, blank=True, null=True)
    url = models.URLField(max_length=200, blank=True, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    version = models.CharField(max_length=100, blank=True, null=True)
    dataset = models.ManyToManyField(Dataset, blank=True)
    def __str__(self):
        return self.name

class Folder(models.Model):
    name = models.CharField(max_length=100, blank=True)
    datePublished = models.DateTimeField(default=datetime.now, blank=True, null=True)
    description = models.CharField(max_length=100, blank=True, null=True)
    keywords = models.CharField(max_length=100, blank=True, null=True)
    url = models.URLField(max_length=200, blank=True, null=True)
    id_labguru = models.CharField(max_length=100, blank=True, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, related_name='folders')
    version = models.CharField(max_length=100, blank=True, null=True)
    experiment = models.ManyToManyField(Experiment, blank=True)
    def __str__(self):
        return self.name

class Project(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    homepage = models.URLField()
    version = models.CharField(max_length=100, blank=True, null=True)
    url = models.URLField()
    id_labguru = models.CharField(max_length=100, blank=True, null=True)
    id_omero = models.CharField(max_length=100, blank=True, null=True)
    id_dmp = models.CharField(max_length=100, blank=True, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_modified = models.DateTimeField(blank=True, null=True)
    keywords = models.CharField(max_length=100, blank=True, null=True)
    link = models.ForeignKey(Link, on_delete=models.CASCADE, blank=True, null=True)
    download = models.ForeignKey(Download, on_delete=models.CASCADE, blank=True, null=True)
    documentation = models.ForeignKey(Documentation, on_delete=models.CASCADE, blank=True, null=True)
    publication = models.ManyToManyField(Publication, blank=True)
    folder = models.ManyToManyField(Folder, blank=True)
 
    def __str__(self):
        return self.name



class Screen(models.Model):
    name = models.CharField(max_length=100, blank=True)
    datePublished = models.DateTimeField(default=datetime.now, blank=True, null=True)
    description = models.CharField(max_length=100, blank=True, null=True)
    keywords = models.CharField(max_length=100, blank=True, null=True)
    url = models.URLField(max_length=200, blank=True, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    version = models.CharField(max_length=100, blank=True, null=True)
    def __str__(self):
        return self.name


