from django.urls import path, include
from openlink_app import views


app_name = 'openlink_app'
urlpatterns = [
    path('', views.home, name='home'),
    path('project/<int:id>/', views.name_project, name='project-detail'),
    path('projects', views.projects, name='projects'),
    path('profile/<username>/', views.get_user_profile),
]