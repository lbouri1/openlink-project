from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.conf import settings
from .filters import ProjectFilter
from .models import Project, Folder, Experiment
from django.contrib.auth.models import User

def home(request):
    return render(request, 'openlink/template.html')

def name_project(request, id):

    a_list = Project.objects.filter(id=id)
    b_list = Folder.objects.filter(project__id=id)
    c_list = Experiment.objects.filter(folder__project__id=id)
    context = {'id': id, 'projects_list': a_list, 'folders_list': b_list, 'experiments_list': c_list}
    return render(request, 'openlink/project.html', context)


def projects(request):
    projects_list = Project.objects.all()
    projects_filter = ProjectFilter(request.GET, queryset=projects_list)
    return render(request, 'openlink/projects_list.html', {'filter': projects_filter})



def get_user_profile(request, username):
    user = User.objects.get(username=username)
    return render(request, 'openlink/user_profile.html', {"user":user})